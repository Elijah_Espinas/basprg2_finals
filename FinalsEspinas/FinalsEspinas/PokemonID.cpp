#include "PokemonID.h"
#include "GenThreePokedex.h"
#include <iostream>
#include <vector>

using namespace std;

pokemonID::pokemonID()
{
}

pokemonID::pokemonID(string pokeName, int level, int currentHp, int maxHp, int baseAtk, int currentExp, int expToNextLvl, int expToGive)
{
	this->pokeName = pokeName;
	this->level = level;
	this->currentHp = currentHp;
	this->maxHp = maxHp;
	this->baseAtk = baseAtk;
	this->currentExp = currentExp;
	this->expToNextLvl = expToNextLvl;
	this->expToGive = expToGive;

	this->playerInventory = {  };
}

void pokemonID::showStat()
{
	cout << "Pokemon Status: " << endl << "=================" << endl << "Name: " << this->pokeName << endl << "Lvl: " << this->level << endl << "HP: " << this->currentHp << "/" << this->maxHp << endl << "Atk: " << this->baseAtk << endl << "EXP: " << this->currentExp << "/" << this->expToNextLvl << endl;
}

void pokemonID::engageOpponent(string pokeName, int currentHp, int maxHp, int baseAtk)
{
	//put function for atk here

}

void pokemonID::lvlUp(string pokeName, int level, int currentHp, int maxHp, int baseAtk, int currentExp, int expToNextLvl, int expToGive)
{
	this->level++;
	cout << "Congratulations! " << this->pokeName << " has leveled up!" << endl << endl << "Pokemon Status: " << endl << "=================" << endl;
	if (this->currentExp > this->expToNextLvl)
	{
		int excessCounter = 0, excessExp = this->currentExp - this->expToNextLvl;
		this->currentExp = 0 + excessExp;
		excessExp = 0;
		excessCounter++;
		if (this->currentExp > this->expToNextLvl)
		{
			while (true)
			{
				excessExp = this->currentExp - this->expToNextLvl;
				this->currentExp = 0 + excessExp;
				excessExp = 0;
				this->level++;
				excessCounter++;
				if (this->currentExp <= this->expToNextLvl)
				{
					break;
				}
				else
				{
					//repeat cycle
				}
			}
		}
		else
		{
			//do nothing
		}
		if (excessCounter > 0)
		{
			for (size_t i = 0; i < excessCounter; i++)
			{
				this->currentHp = this->maxHp;
				this->maxHp = this->maxHp * 1.15;
				this->baseAtk = this->baseAtk * 1.10;
				this->expToNextLvl = 1.20;
				this->expToGive = this->expToGive * 1.20;
			}
			cout << "Name: " << this->pokeName << endl << "Lvl: " << this->level << endl << "HP: " << this->currentHp << "/" << this->maxHp << endl << "Atk: " << this->baseAtk << "EXP: " << this->currentExp << "/" << this->expToNextLvl << endl;
		}
		else
		{
			this->currentHp = this->maxHp;
			this->maxHp = this->maxHp * 1.15;
			this->baseAtk = this->baseAtk * 1.10;
			this->expToNextLvl = 1.20;
			this->expToGive = this->expToGive * 1.20;
			cout << "Name: " << this->pokeName << endl << "Lvl: " << this->level << endl << "HP: " << this->currentHp << "/" << this->maxHp << endl << "Atk: " << this->baseAtk << "EXP: " << this->currentExp << "/" << this->expToNextLvl << endl;
		}
		
	}
	else
	{
		this->currentHp = this->maxHp;
		this->maxHp = this->maxHp * 1.15;
		this->baseAtk = this->baseAtk * 1.10;
		this->currentExp = 0;
		this->expToNextLvl = 1.20;
		this->expToGive = this->expToGive * 1.20;
		cout << "Name: " << this->pokeName << endl << "Lvl: " << this->level << endl << "HP: " << this->currentHp << "/" << this->maxHp << endl << "Atk: " << this->baseAtk << "EXP: " << this->currentExp << "/" << this->expToNextLvl << endl;
	}
}

