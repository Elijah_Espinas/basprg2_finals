#pragma once
#include "PokemonID.h"

using namespace std;

class pokeBattle
{
public:
	pokeBattle(int encRate, int pokeListRoll, int hitRate, int capRate);
	int encRate = 40;
	int pokeListRoll;
	int hitRate = 80;
	int capRate = 30;
	void rollEncounter(int encRate, int pokeListRoll);
	void battleStart(int hitRate, int capRate);
	pokeBattle();
};