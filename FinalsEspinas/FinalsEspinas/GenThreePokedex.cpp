#include "GenThreePokedex.h"


genThreePokedex::genThreePokedex()
{
	// guide to sorting {name, level, currentHp, maxHp, atk, currentExp, expToNextLvl, expToGive}
	//starter pokemon
	pokemonID* pokemon = new pokemonID();
	
	pokemonID* Treecko = new pokemonID("Treecko", 5, 40, 40, 17, 0, 160, 23);
	pokemonID* Grovyle = new pokemonID("Grovyle", 16, 80, 80, 34, 0, 320, 46);
	pokemonID* Sceptile = new pokemonID("Sceptile", 36, 160, 160, 68, 0, 640, 92);

	pokemonID* Torchic = new pokemonID("Torchic", 5, 40, 40, 17, 0, 160, 23);
	pokemonID* Combusken = new pokemonID("Combusken", 16, 80, 80, 34, 0, 320, 46);
	pokemonID* Blaziken = new pokemonID("Blaziken", 36, 160, 160, 68, 0, 640, 92);

	pokemonID* Mudkip = new pokemonID("Mudkip", 5, 40, 40, 17, 0, 160, 23);
	pokemonID* Marshtomp = new pokemonID("Marshtomp", 16, 80, 80, 34, 0, 320, 46);
	pokemonID* Swampert = new pokemonID("Swampert", 36, 160, 160, 68, 0, 640, 92);


	//introductory tier pokemon
	pokemonID* Poochyena = new pokemonID("Poochyena", 1, 100, 100, 34, 0, 130, 12);
	pokemonID* Mightyena = new pokemonID("Mightyena", 18, 100, 100, 34, 0, 450, 24);

	pokemonID* Zigzagoon = new pokemonID("Zigzagoon", 1, 100, 100, 34, 0, 110, 10);
	pokemonID* Linoone = new pokemonID("Linoone", 20, 100, 100, 34, 0, 380, 20);
	
	pokemonID* Wurmple = new pokemonID("Wurmple", 1, 100, 100, 34, 0, 105, 8);
	pokemonID* Silcoon = new pokemonID("Silcoon", 7, 100, 100, 34, 0, 230, 16);
	pokemonID* Beautifly = new pokemonID("Beautifly", 10, 100, 100, 34, 0, 430, 30);
	pokemonID* Cascoon = new pokemonID("Cascoon", 7, 100, 100, 34, 0, 240, 16);
	pokemonID* Dustox = new pokemonID("Dustox", 10, 100, 100, 34, 0, 510, 30);
	
	pokemonID* Lotad = new pokemonID("Lotad", 1, 100, 100, 34, 0, 110, 8);
	pokemonID* Lombre = new pokemonID("Lombre", 14, 100, 100, 34, 0, 320, 18);
	pokemonID* Ludicolo = new pokemonID("Ludicolo", 20, 100, 100, 34, 0, 630, 34);
	
	pokemonID* Seedot = new pokemonID("Seedot", 1, 100, 100, 34, 0, 110, 12);
	pokemonID* Nuzleaf = new pokemonID("Nuzleaf", 14, 100, 100, 34, 0, 250, 24);
	pokemonID* Shiftry = new pokemonID("Shiftry", 20, 100, 100, 34, 0, 610, 48);
	
	pokemonID* Taillow = new pokemonID("Taillow", 1, 100, 100, 34, 0, 100, 10);
	pokemonID* Swellow = new pokemonID("Swellow", 22, 100, 100, 34, 0, 100, 25);


	//intermediate tier pokemon
	pokemonID* Wingull = new pokemonID("Wingull", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Pelipper = new pokemonID("Pelipper", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Ralts = new pokemonID("Ralts", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Kirlia = new pokemonID("Kirlia", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Gardevoir = new pokemonID("Gardevoir", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Surskit = new pokemonID("Surskit", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Masquerain = new pokemonID("Masquerain", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Shroomish = new pokemonID("Shroomish", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Breloom = new pokemonID("Breloom", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Slakoth = new pokemonID("Slakoth", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Vigoroth = new pokemonID("Vigoroth", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Slaking = new pokemonID("Slacking", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Nincada = new pokemonID("Nincada", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Shedinja = new pokemonID("Shedinja", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Whismur = new pokemonID("Whismur", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Loudred = new pokemonID("Loudred", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Exploud = new pokemonID("Exploud", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Makuhita = new pokemonID("Makuhita", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Hariyama = new pokemonID("Hariyama", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Azurill = new pokemonID("Azurill", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Nosepass = new pokemonID("Nosepass", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Skitty = new pokemonID("Skitty", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Delcatty = new pokemonID("Delcatty", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Sableye = new pokemonID("Sableye", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Mawile = new pokemonID("Mawile", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Aron = new pokemonID("Aron", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Lairon = new pokemonID("Lairon", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Aggron = new pokemonID("Aggron", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Meditite = new pokemonID("Meditite", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Medicham = new pokemonID("Medicham", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Electrike = new pokemonID("Electrike", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Manectric = new pokemonID("Manectric", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Plusle = new pokemonID("Plusle", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Minun = new pokemonID("Minun", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Volbeat = new pokemonID("Volbeat", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Illumise = new pokemonID("Illumise", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Roselia = new pokemonID("Roselia", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Gulpin = new pokemonID("Gulpin", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Swalot = new pokemonID("Swalot", 5, 100, 100, 34, 90, 100, 23);


	//advanced tier pokemon
	pokemonID* Carvanha = new pokemonID("Carvanha", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Sharpedo = new pokemonID("Sharpedo", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Wailmer = new pokemonID("Wailmer", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Wailord = new pokemonID("Wailord", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Numel = new pokemonID("Numel", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Camerupt = new pokemonID("Camerupt", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Torkoal = new pokemonID("Torkoal", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Sproink = new pokemonID("Sproink", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Grumpig = new pokemonID("Grumpig", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Spinda = new pokemonID("Spinda", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Trapinch = new pokemonID("Trapinch", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Vibrava = new pokemonID("Vibrava", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Flygon = new pokemonID("Flygon", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Cacnea = new pokemonID("Cacnea", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Cacturne = new pokemonID("Cacturne", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Swablu = new pokemonID("Swablu", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Altaria = new pokemonID("Altaria", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Zangoose = new pokemonID("Zangoose", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Seviper = new pokemonID("Seviper", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Lunatone = new pokemonID("Lunatone", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Solrock = new pokemonID("Solrock", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Barboach = new pokemonID("Barboach", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Whiscash = new pokemonID("Whiscash", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Corphish = new pokemonID("Corphish", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Crawdaunt = new pokemonID("Crawdaunt", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Baltoy = new pokemonID("Baltoy", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Claydol = new pokemonID("Claydol", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Lileep = new pokemonID("Lileep", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Cradily = new pokemonID("Cradily", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Anorith = new pokemonID("Anorith", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Armaldo = new pokemonID("Armaldo", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Feebas = new pokemonID("Feebas", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Milotic = new pokemonID("Milotic", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Castform = new pokemonID("Castform", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Kecleon = new pokemonID("Kecleon", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Shuppet = new pokemonID("Shuppet", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Banette = new pokemonID("Banette", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Duskull = new pokemonID("Duskull", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Dusclops = new pokemonID("Dusclops", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Tropius = new pokemonID("Tropius", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Chimecho = new pokemonID("Chimecho", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Absol = new pokemonID("Absol", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Wynaut = new pokemonID("Wynaut", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Snorunt = new pokemonID("Snorunt", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Glalie = new pokemonID("Glalie", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Spheal = new pokemonID("Spheal", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Sealeo = new pokemonID("Sealeo", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Walrein = new pokemonID("Walrein", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Clamperl = new pokemonID("Clamperl", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Huntail = new pokemonID("Huntail", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Gorebyss = new pokemonID("Gorebyss", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Relicanth = new pokemonID("Relicanth", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Luvdisc = new pokemonID("Luvdisc", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Bagon = new pokemonID("Bagon", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Shelgon = new pokemonID("Shelgon", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Salamence = new pokemonID("Salamence", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Beldum = new pokemonID("Beldum", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Metang = new pokemonID("Metang", 5, 100, 100, 34, 90, 100, 23);
	pokemonID* Metagross = new pokemonID("Metagross", 5, 100, 100, 34, 90, 100, 23);


	//legendary list
	pokemonID* Regirock = new pokemonID("Regirock", 60, 240, 240, 50, 0, 100, 2000);
	pokemonID* Regice = new pokemonID("Regice", 60, 240, 240, 50, 0, 100, 2000);
	pokemonID* Registeel = new pokemonID("Registeel", 60, 240, 240, 50, 0, 100, 2000);

	pokemonID* Latias = new pokemonID("Latias", 65, 200, 200, 45, 0, 100, 2000);
	pokemonID* Latios = new pokemonID("Latios", 65, 200, 200, 45, 0, 100, 2000);

	pokemonID* Kyogre = new pokemonID("Kyogre", 70, 320, 320, 60, 0, 100, 3000);
	pokemonID* Groudon = new pokemonID("Groudon", 70, 320, 320, 60, 0, 100, 3000);
	pokemonID* Rayquaza = new pokemonID("Rayquaza", 70, 330, 330, 70, 0, 100, 3000);

	pokemonID* Jirachi = new pokemonID("Jirachi", 70, 320, 320, 45, 0, 100, 2500);
	pokemonID* Deoxys = new pokemonID("Deoxys", 70, 180, 180, 60, 0, 100, 2500); 

	//this is for spawning pokemon in certain areas
	this->starterList = { Treecko, Torchic, Mudkip };
	this->pokemonIntroductoryList = { Poochyena, Zigzagoon, Wurmple, Silcoon, Cascoon, Lotad, Seedot, Taillow };
	this->pokemonIntermediateList = {  };
	this->pokemonAdvancedList = {  };
}
