#include <string>
#include <iostream>
#include <conio.h>
#include <vector>
#include "PokemonID.h"
#include "GenThreePokedex.h"
#include "HoennMap.h"
#include "PokeBattle.h"

using namespace std;

int main()
{

	genThreePokedex* pokedex = new genThreePokedex();
	hoennMap* map = new hoennMap();
	pokeBattle* battle = new pokeBattle();
	pokemonID* pokemon = new pokemonID();

	string profName = "Proffesor Birch", playerName, playerInput;
	int pokemonStatusChecker;

	cout << profName << ": Hello there youngster! Welcome to the world of Pokemon, where Trainers explore the world to catch and train Pokemon to participate in battles and competitions against other Trainers." << endl;
	_getch();
	system("cls");

	cout << profName << ": So, tell me... What's your name?" << endl;
	cin >> playerName;
	_getch();
	system("cls");

	cout << profName << ": Ahh, a fine name for a youngster. Well then, let's get down to business, eh?";
	_getch();
	system("cls");

	cout << profName << ": I have here three Pokeballs, each containing a Pokemon who will accompany you on your adventure. You can take a look at each one of them while you're deciding who to pick." << endl;
	_getch();
	system("cls");

	//still have to implement a vector that will add the chosen pokemon into an inventory
	//then implement a counter in said vector that will check for active and fainted pokemon (ei hp == 0) for use in battle
	while (true)
	{
		int confirmation = 0;
		cout << "Select a pokemon you wish to inspect:" << endl << endl << "[1] Treecko" << endl << "[2] Torchic" << endl << "[3] Mudkip" << endl << endl;
		cin >> playerInput;
		if (playerInput == "1")
		{
			pokedex->starterList[0]->showStat();
			cout << endl << endl << "Do you want to select this pokemon?" << endl << "[Y]es		[N]o" << endl;
			cin >> playerInput;
			while (true)
			{
				if (playerInput == "y" || playerInput == "Y")
				{
					cout << "You chose " << pokedex->starterList[0]->pokeName << " as your starter!" << endl;
					pokemon->playerInventory.push_back(pokedex->starterList[0]);
					confirmation++;
					break;
				}
				else if (playerInput == "n" || playerInput == "N")
				{
					cout << "Select another pokemon you wish to view." << endl;
					break;
				}
				else
				{
					//repeat
				}
			}
		}
		else if (playerInput == "2")
		{
			pokedex->starterList[1]->showStat();
			cout << endl;
			cout << endl << endl << "Do you want to select this pokemon?" << endl << "[Y]es		[N]o" << endl;
			cin >> playerInput;
			while (true)
			{
				if (playerInput == "y" || playerInput == "Y")
				{
					cout << "You chose " << pokedex->starterList[1]->pokeName << " as your starter!" << endl;
					pokemon->playerInventory.push_back(pokedex->starterList[1]);
					confirmation++;
					break;
				}
				else if (playerInput == "n" || playerInput == "N")
				{
					cout << "Select another pokemon you wish to view." << endl;
					break;
				}
				else
				{
					//repeat
				}
			}
		}
		else if (playerInput == "3")
		{
			pokedex->starterList[2]->showStat();
			cout << endl;
			cout << endl << endl << "Do you want to select this pokemon?" << endl << "[Y]es		[N]o" << endl;
			cin >> playerInput;
			while (true)
			{
				if (playerInput == "y" || playerInput == "Y")
				{
					cout << "You chose " << pokedex->starterList[2]->pokeName << " as your starter!" << endl;
					pokemon->playerInventory.push_back(pokedex->starterList[2]);
					confirmation++;
					break;
				}
				else if (playerInput == "n" || playerInput == "N")
				{
					cout << "Select another pokemon you wish to view." << endl;
					break;
				}
				else
				{
					//repeat
				}
			}
		}
		else
		{
			//repeat statement
		}

		if (confirmation > 0 )
		{
			break;
		}
		else
		{
			//continue with loop
		}

	}
	_getch();
	system("cls");

	cout << profName << ": Great! Now you're all set on your journey with your chosen partner. Alright then!";
	_getch();
	system("cls");

	cout << profName << ": Go out there and start your Pokemon adventure! Good luck, kiddo!";
	_getch();
	system("cls");

	cout << ".";
	_getch();
	system("cls");

	cout << ". .";
	_getch();
	system("cls");

	cout << ". . .";
	_getch();
	system("cls");

	//while loop is for the rest of the game (walking, battles, interactions, etc.)
	while (true)
	{
		
		cout << "You are currently on " << map->xCoordinate << "," << map->yCoordinate << endl;
		map->playerMovement();
		_getch();
		system("cls");
		if (map->xCoordinate && map->yCoordinate == map->mapList[1]->xCoordinate && map->mapList[1]->yCoordinate)
		{
			cout << "You are currently on " << map->mapList[1]->areaName << ". " << map->mapList[1]->pokeFacility << endl << "What would you like to do:	[H]eal Pokemon		[K]eep Walking" << endl;
			cin >> playerInput;
			while (true)
			{
				if (playerInput == "h" || playerInput == "H")
				{
					for (size_t i = 0; i < pokemon->playerInventory.size(); i++)
					{
						pokemon->playerInventory[i]->currentHp = pokemon->playerInventory[i]->maxHp;
					}
					cout << "You have healed all of your pokemon.";
					_getch();
					system("cls");
				}
				else if (playerInput == "k" || playerInput == "K" )
				{
					break;
				}
				else
				{
					//repeat loop
				}
			}
			_getch();
			system("cls");
		}
		else if (map->xCoordinate && map->yCoordinate == map->mapList[1]->xCoordinate && map->mapList[2]->yCoordinate)
		{
			cout << "You are currently on " << map->mapList[1]->areaName << ". " << map->mapList[2]->pokeFacility << endl << "What would you like to do:	[H]eal Pokemon		[K]eep Walking" << endl;
			cin >> playerInput;
			while (true)
			{
				if (playerInput == "h" || playerInput == "H")
				{
					for (size_t i = 0; i < pokemon->playerInventory.size(); i++)
					{
						pokemon->playerInventory[i]->currentHp = pokemon->playerInventory[i]->maxHp;
					}
					cout << "You have healed all of your pokemon.";
					_getch();
					system("cls");
				}
				else if (playerInput == "k" || playerInput == "K")
				{
					break;
				}
				else
				{
					//repeat loop
				}
			}
			_getch();
			system("cls");
		}
		else
		{
			//repeat loop
		}
	}

	/*

	//random pokemon encounter for reference
	int randomEncounterChance, randomPokemon;

	randomEncounterChance = rand() % 100 + 1;
	if (randomEncounterChance <= 40) //if true, then you have encountered a pokemon
	{

		cout << "You have encountered a wild Pokemon!" << endl;

		randomPokemon = rand() % (pokedex->starterList.size()) + 1;

	}

	else
		{
			cout << "You saw nothing." << endl;
		}

	_getch();
	system("cls");

	*/

	_getch();
	system("cls");
}