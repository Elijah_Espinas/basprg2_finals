#pragma once
#include "Pokebattle.h"
#include "GenThreePokedex.h"

using namespace std;

class hoennMap
{
public:
	hoennMap(string areaName, string pokeFacility, int xCoordinate, int yCoordinate);
	string areaName;
	string pokeFacility;
	int xCoordinate = 0;
	int yCoordinate = 0;
	void playerMovement(/* int xCoordinate, int yCoordinate */);
	void areaOrientation(string areaName, string pokeFacility, int xCoordinate, int yCoordinate);

	vector<hoennMap*> mapList;
	hoennMap();
};
