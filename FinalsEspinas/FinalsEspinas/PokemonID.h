#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <random>

using namespace std;

class pokemonID
{
public:
	pokemonID();
	pokemonID(string pokeName, int level, int currentHp, int maxHp, int baseAtk, int currentExp, int expToNextLvl, int expToGive);
	//pokemonID* pokemon = new pokemonID(pokeName, level, currentHp, maxHp, baseAtk, currentExp, expToNextLvl, expToGive);
	string pokeName;
	int level;
	int currentHp;
	int maxHp;
	int baseAtk;
	int currentExp;
	int expToNextLvl;
	int expToGive;

	vector<pokemonID*> playerInventory;

	void showStat();//string pokeName, int level, int currentHp, int maxHp, int baseAtk, int currentExp, int expToNextLvl);
	void engageOpponent(string pokeName, int currentHp, int maxHp, int baseAtk);
	void lvlUp(string pokeName, int level, int currentHp, int maxHp, int baseAtk, int currentExp, int expToNextLvl, int expToGive);
};
