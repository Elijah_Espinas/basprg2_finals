#pragma once
#include "PokemonID.h"

using namespace std;

class genThreePokedex
{
public:
	genThreePokedex();

	vector<pokemonID*> starterList;
	vector<pokemonID*> pokemonIntroductoryList;
	vector<pokemonID*> pokemonIntermediateList;
	vector<pokemonID*> pokemonAdvancedList;
};
